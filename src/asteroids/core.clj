(ns asteroids.core
  (require [asteroids.graph :as g]))

(def case1
  [{:id 0 :p [0 0 0] :v [0 0 0]}
   {:id 1 :p [5 0 0] :v [0 0 0]}
   {:id 2 :p [10 1 0] :v [-1 0 0]}])

(def case2
  [{:id 0 :p [0 0 0] :v [1 0 0]}
   {:id 1 :p [0 1 0] :v [0 -1 0]}
   {:id 2 :p [1 1 1] :v [3 1 1]}
   {:id 3 :p [-1 -1 2] :v [1 -1 -1]}])

(def case3
  [{:id 0 :p [0 0 0] :v [0 0 0]}
   {:id 1 :p [0.5 0 0] :v [0 0 0]}
   {:id 2 :p [0.5 1 0] :v [-100 0 0]}])

(def case4
  [{:id 0 :p [0 0 0] :v [0 0 0]}
   {:id 1 :p [5 0 0] :v [0 0 0]}
   {:id 2 :p [10 1 0] :v [-10 0 0]}])

(def vertex
  [1 2 3])

(def vertex-edge
  #{[0 0 0] [0 1 0]})

(def id-edge
  #{0 1})

(def base
  {:id 0
   :p [1 2 3]
   :v [2 -1 0]})

(def base2
  {:id 1
   :p [2 2 3]
   :v [2 -1 0]})

(def link
  {:id 1
   :edge #{base base2}
   :dist 1
   :delta 0.001})

(def tinc 1e-6)

(def world (atom nil))

(def link-ids (atom 0))

(def net-calcs (atom 0))

(defn reset-stats! []
  (reset! net-calcs 0))

(defn edge-dist [edge]
  (apply g/dist (map :p edge)))

(defn update-dists
  "Calculates the distance of each link and updates the links."
  [links]
  (map
    #(assoc % :dist (edge-dist (:edge %)))
    links))

(defn all-links
  "Finds all of the possible links connecting any two of the provide bases."
  [bases]
  (->> (for [b1 bases
             b2 bases]
         {:id (swap! link-ids inc)
          :edge (set [b1 b2])})
    distinct
    (keep #(when (= 2 (count (:edge %))) %))
    update-dists
    (sort-by :dist)))

(defn edges-to-ids [bases edges]
  (let [idlookup (reduce #(assoc %1 (:p %2) (:id %2)) {} bases)]
    (map #(set (map idlookup %)) edges)))

(defn opt-network
  "Find the optimal network (minimum spanning tree) connecting all of the bases."
  [bases]
  (swap! net-calcs inc)
  (->> (g/mst (map :p bases) g/dist)
    (edges-to-ids bases)
    set))

(defn move-base [base]
  (assoc base
    :p (vec (map #(+ %1 (* tinc %2)) (:p base) (:v base)))))

(defn move-bases [world]
  (assoc world
    :bases (map move-base (:bases world))
    :t (+ tinc (:t world))))

(defn add-deltas [prev-links links]
  (let [prev-dists (reduce #(assoc %1 (:id %2) (:dist %2)) {} prev-links)]
    (map #(assoc % :delta (- (:dist %) (get prev-dists (:id %)))) links)))

(defn swap-link-bases [bases links]
  (let [base-map (reduce #(assoc %1 (:id %2) %2) {} bases)
        swap-base #(get base-map (:id %))]
    (map
      #(assoc % :edge (map swap-base (:edge %)))
      links)))

(defn update-links [world]
  (let [prev-links (:links world)]
    (assoc world
      :prev-links prev-links
      :links (->> prev-links
               (swap-link-bases (:bases world))
               (update-dists)
               (add-deltas prev-links)
               (sort-by :dist)))))

(defn change-possible? [world]
  (not=
    (map :id (:prev-links world))
    (map :id (:links world))))

(defn update-network [world]
  (if (change-possible? world)
    (let [new-network (opt-network (:bases world))]
      (if (not= (:network world) new-network)
        (assoc world
          :network new-network
          :network-ver (inc (:network-ver world)))
        world))
    world))

(defn update-world [world]
  (-> world
    move-bases
    update-links
    update-network))

(defn update-world! []
  (swap! world update-world))

(defn init-world! [bases]
  (reset-stats!)
  (reset! link-ids 0)
  (reset!
    world
    {:t 0
     :bases bases
     :prev-links nil
     :links (all-links bases)
     :network (opt-network bases)
     :network-ver 1}))

(defn unstable?
  "Will the optimal network change in the future?

  Assume the initial state is unstable.

  If any link distances are decreasing, the network is not stable.

  If all link distances are static or increasing, determined by comparing the order of the links
  sorted by distance to the order of the links sorted by rate of change in distance. If the orders
  are different, the network is unstable."
  [world]
  (let [links (:links world)]
    (or
      (nil? (:prev-links world))
      (some neg? (map :delta links))
      (= (map :id links) (map :id (sort-by :delta links))))))

(defn network-changes [bases]
  (do
    (init-world! bases)
    (while (unstable? @world) (update-world!))
    (:network-ver @world)))