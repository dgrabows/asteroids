(ns asteroids.graph
  (:require [clojure.set :as set])
  (:import [java.lang.Math]))

;(defn dist
;  "Calculates the distance between two vertices."
;  [v1 v2]
;  (Math/sqrt
;    (apply +
;      (map #(Math/pow (- %1 %2) 2) v1 v2))))

(defn dist
  "Calculates the distance between two vertices."
  [v1 v2]
  (apply +
    (map #(Math/pow (- %1 %2) 2) v1 v2)))

(defn cost-matrix
  "Build matrix of costs for all possible edges between provided vertices."
  [verts costfn]
  (apply merge-with merge
    (for [v1 verts
          v2 verts]
      {v1 {v2 (costfn v1 v2)}})))

(defn min-edge
  "Finds the minimum cost edge that connects a vertex in vnew to a vertex in v that is
  not already in vnew."
  [v vnew costs]
  (reduce
    #(if (< (get-in costs %1) (get-in costs %2)) %1 %2)
    (for [v1 vnew
          v2 (set/difference v vnew)]
      #{v1 v2})))

(defn mst
  "Finds the minimum spanning tree for the provided set of vertices and cost function.
  The cost function must take two vertices as arguments. Returns a set of edges."
  [verts costfn]
  (let [v (set verts)
        costs (cost-matrix verts costfn)]
    (loop [vnew #{(first verts)}
           enew #{}]
      (if (= v vnew)
        enew
        (let [new-edge (min-edge v vnew costs)]
          (recur
            (set/union vnew new-edge)
            (conj enew new-edge)))))))

