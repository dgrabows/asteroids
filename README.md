# asteroids

Attempt to solve the following programming problem found here: http://icpc.baylor.edu/digital/data/icpc2012.pdf

## Approach

Represent the asteroids as a graph where the cost of the edges is the distance between the nodes.

Find the optimal communication network for any configuration by calculating the minimum spanning tree. See
[Prim's algorithm](http://en.wikipedia.org/wiki/Prim%27s_algorithm) for details on the approached used to do this.

To determine if the optimal network configuration has changed from one iteration to the next, sort the potential
edges of the graph by cost/distance. If the order of the sorted edges does not change, the optimal network remains
the same.

To determine if the optimal network has reached a stable configuration, calculate the rate of change in the length
of each edge in the graph. If there are no edges decreasing in length and order of the edges sorted by rate of change
matches the order of the edges sorted by length, then a stable state has been reached.

## Usage

FIXME

## License

Copyright © 2012 Dan Grabowski

Distributed under the [Eclipse Public License](http://www.eclipse.org/legal/epl-v10.html).
